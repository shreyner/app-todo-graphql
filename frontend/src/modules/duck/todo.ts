import {AnyAction} from 'redux';

export interface Todo {
  id: string;
  text: string;
  completed: boolean;
}

/// Actions
export const TODO_ADD = 'app/todo/add';
export const TODO_FETCH_REQUEST_CREATE = 'app/todo-fetch-request/create';

export const TODO_UPDATE = 'app/todo/update';
export const TODO_UPDATE_ITEM = 'app/todo/update-item';

export const TODO_CHANGE_STATUS_COMPLETE = 'app/todo/CHANGE_STATUS_COMPLETE';

export const TODO_CLEAR_COMPLETED = 'app/todo/clear-completed';
export const TODO_DELETED_BY_IDS = 'app/todo/deleted-by-ids';

const initStore: ReadonlyArray<Todo> = [];

/// Reducers
export default (store: ReadonlyArray<Todo> = initStore, action: AnyAction) => {
  switch (action.type) {
    case TODO_UPDATE:
      return action.payload;
    case TODO_UPDATE_ITEM:
      return store.map(item => item.id === action.todo.id ? action.todo : item);
    case TODO_CHANGE_STATUS_COMPLETE:
      return store.map(item => item.id === action.id ? {...item, completed: !item.completed} : item);
    case TODO_ADD:
      return !store.find(({id}) => id === action.todo.id) ? [...store, action.todo] : store;
    case TODO_CLEAR_COMPLETED:
      // added delete by id
      return store.filter(item => !item.completed);
    case TODO_DELETED_BY_IDS:
      return store.filter(item => !action.ids.includes(item.id));
    default:
      return store;
  }
};

// Actions Request
export const fetchRequestAddTodo = (text: string) => ({type: TODO_FETCH_REQUEST_CREATE, text});

// Actions
export const updateTodo = (payload: ReadonlyArray<Todo>) => ({type: TODO_UPDATE, payload});
export const updateTodoItem = (todo: Todo) => ({type: TODO_UPDATE_ITEM, todo})

export const changeCompleteTodo = (id: string) => ({type: TODO_CHANGE_STATUS_COMPLETE, id});

export const addTodo = (todo: Todo) => ({type: TODO_ADD, todo});

export const clearCompleted = () => ({type: TODO_CLEAR_COMPLETED});
export const todoDeleteByIds = (ids: ReadonlyArray<string>) => ({type: TODO_DELETED_BY_IDS, ids});
