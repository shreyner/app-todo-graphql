import {AnyAction} from 'redux';

export interface LocationStore {
  // type: string;
}

const initialStore: LocationStore = {

};

export default (store = initialStore, action: AnyAction) => {
  switch (action.type) {
    default:
      return store;
  }
};

/// Constants
export const NOT_FOUND = '@navigation/not_found';
