import {eventChannel} from 'redux-saga';
import {AnyAction} from 'redux';
import {call, fork, put, take, takeEvery, takeLatest} from 'redux-saga/effects';
import {router} from 'redux-saga-router';

import {navigationActions, routesSaga} from '../../routes/routes';
import {
  addTodo,
  TODO_CHANGE_STATUS_COMPLETE,
  TODO_CLEAR_COMPLETED,
  TODO_FETCH_REQUEST_CREATE,
  todoDeleteByIds,
  updateTodo,
  updateTodoItem
} from '../duck/todo';
import {
  subscribeTodoAdded,
  subscribeTodoDeleted,
  subscribeTodoUpdate,
  fetchAddTodo,
  fetchChangeCompleted,
  fetchClearCompletedTodo,
  fetchTodoList
} from '../../services/todo';

function* loadingTodo() {
  const list = yield call(fetchTodoList);
  yield put(updateTodo(list));
}

function* completedTodo({id}: AnyAction) {
  yield call(fetchChangeCompleted, id);
}

function* clearCompletedTodo() {
  yield call(fetchClearCompletedTodo);
}

function* createFetchTodo({text}: AnyAction) {
  const todo = yield call(fetchAddTodo, text);
  yield put(addTodo(todo));
}

function* subscriptionsTodo() {
  return eventChannel(emit => {
    subscribeTodoAdded.subscribe(data => emit(addTodo(data)));
    subscribeTodoDeleted.subscribe(data => emit(todoDeleteByIds(data)));
    subscribeTodoUpdate.subscribe(data => emit(updateTodoItem(data)));

    return () => {
      console.info('Unsubscribe');
    };
  });
}

function* flowSubscribeTodo() {
  const channel = yield call(subscriptionsTodo);
  while (true) {
    const action = yield take(channel);
    yield put(action);
  }
}

export function initMainSaga(history: any) {
  return function* mainSaga() {
    yield takeLatest(navigationActions.TODO_INDEX, loadingTodo);

    yield takeEvery(TODO_CHANGE_STATUS_COMPLETE, completedTodo);
    yield takeEvery(TODO_CLEAR_COMPLETED, clearCompletedTodo);
    yield takeEvery(TODO_FETCH_REQUEST_CREATE, createFetchTodo);

    // const data = yield call(fetchInitialData);

    // yield put(ready(data));

    // The recommended way is to `fork` the router, but you can delegate with
    // yield* too

    yield fork(router, history, routesSaga);
    yield fork(flowSubscribeTodo);
  };
}
