import {OperationDefinitionNode} from 'graphql';
import {ApolloClient} from 'apollo-client';
import {split} from 'apollo-link';
import {HttpLink} from 'apollo-link-http';
import {WebSocketLink} from 'apollo-link-ws';
import {getMainDefinition} from 'apollo-utilities';
import {InMemoryCache} from 'apollo-cache-inmemory';

export {default as gql} from 'graphql-tag';

// Create an http link:
const httpLink = new HttpLink({
  uri: 'http://localhost:4000',
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: 'ws://localhost:4000/graphql',
  options: {
    reconnect: true,
  },
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
// split based on operation type

const link = split(
  ({query}) => {
    const {kind, operation} = getMainDefinition(query) as OperationDefinitionNode;
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

const cache = new InMemoryCache({
  addTypename: false,
});

export default new ApolloClient({
  link,
  cache,
});
