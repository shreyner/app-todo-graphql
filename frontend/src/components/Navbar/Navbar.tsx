import * as React from 'react';
import {Menu} from 'antd';
import Link from 'redux-first-router-link';
import {navigationActions} from '../../routes/routes';

export interface NavbarProps {
}

export default class extends React.Component<NavbarProps> {
  render(): React.ReactNode {
    return (
      <Menu
        theme='light'
        mode='horizontal'
        style={{lineHeight: '64px'}}
      >
        <Menu.Item>
          <Link to={{type: navigationActions.ROOT}}>Home</Link>
        </Menu.Item>
        <Menu.Item>
          <Link to={{type: navigationActions.TODO_INDEX}}>Todo app</Link>
        </Menu.Item>
      </Menu>
    );
  }
}
