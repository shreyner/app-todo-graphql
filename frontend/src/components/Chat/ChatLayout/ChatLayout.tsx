import * as React from 'react';

export interface ChatLayoutProps {
}

export default class extends React.Component<ChatLayoutProps> {
  render(): React.ReactNode {
    return (
      <div
        style={{
          display: 'flex',
          flexFlow: 'column nowrap',
          height: '100%',
          border: '1px solid silver',
          backgroundColor: '#fff',
          borderRadius: '4px',
        }}
      >
        {this.props.children}
      </div>
    );
  }
}
