import * as React from 'react';

import {Todo} from '../../modules/duck/todo';
import TodoItem from './TodoItem/TodoItem';

export interface TodoListProps {
  todoList: ReadonlyArray<Todo>;
  completeTodo: (id: string) => void;
}

const TodoList: React.SFC<TodoListProps> = ({todoList, completeTodo}) => (
  <ul>
    {todoList.map((item) => <TodoItem key={item.id} todo={item} onClick={completeTodo}/>)}
  </ul>
);

export default TodoList;
