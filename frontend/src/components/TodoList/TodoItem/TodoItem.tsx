import * as React from 'react';
import {Todo} from '../../../modules/duck/todo';

export interface TodoItemProps {
  todo: Todo;
  onClick: (id: string) => void;
}

export default class extends React.Component<TodoItemProps> {
  render(): React.ReactNode {
    const {todo: {completed, text}} = this.props;
    return (
      <li style={{textDecoration: completed ? 'line-through' : ''}} onClick={this.handleClick}>{text}</li>
    );
  }

  handleClick = () => this.props.onClick ? this.props.onClick(this.props.todo.id) : void 0;
}
