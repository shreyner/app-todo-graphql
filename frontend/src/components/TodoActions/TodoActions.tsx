import * as React from 'react';
import {Button, Input} from 'antd';

const ButtonGroup = Button.Group;

export interface TodoFormProps {
  value: string;
  onChange: (text: string) => void;
  onAdd: () => any;
  clearCompleted: () => any;
}

export default class extends React.Component<TodoFormProps> {
  render(): React.ReactNode {
    const {value, onAdd, clearCompleted} = this.props;

    return (
      <Input.Group>
        <Input type='text' style={{width: '200px'}} value={value} onChange={this.handleChange}/>
        <ButtonGroup>
          <Button
            type='primary'
            onClick={onAdd}
            style={{borderTopLeftRadius: '0', borderBottomLeftRadius: '0'}}>
            Add
          </Button>
          <Button type='primary' onClick={clearCompleted}>Clear completed</Button>
        </ButtonGroup>
      </Input.Group>
    );
  }

  handleChange = (event: any) => {
    const value = event.target.value;
    this.props.onChange(value);
  }
}
