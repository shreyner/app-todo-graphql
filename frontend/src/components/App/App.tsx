import * as React from 'react';

import RootLayout from '../../layout/Root';
import TodoPage from '../../scenes/Todo/Todo';
import PointPokerPage from '../../scenes/PointPoker/PointPoker';
import CreateSessionPage from '../../scenes/CreateSession/CreateSession';
import {navigationActions} from '../../routes/routes';

export interface AppProps {
  typeNavigation: string;
}

export default class extends React.Component<AppProps> {
  render(): React.ReactNode {
    return (
      <RootLayout>
        {this.renderContent(this.props.typeNavigation)}
      </RootLayout>
    );
  }

  renderContent(typeNavigation: string) {
    switch (typeNavigation) {
      case navigationActions.TODO_INDEX:
      case navigationActions.TODO_COMPLETED:
      case navigationActions.TODO_NOT_COMPLETED:
      case navigationActions.TODO_SUCH:
        return <TodoPage typeNavigation={this.props.typeNavigation}/>;
      case navigationActions.ROOT:
        return <PointPokerPage/>;
      case navigationActions.POINT_POKER:
        return <CreateSessionPage />;
      default:
        return '';
    }
  }
}
