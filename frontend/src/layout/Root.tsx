import * as React from 'react';
import {Layout} from 'antd';

import Navbar from '../components/Navbar/Navbar';

const {Header, Content} = Layout;

export default class extends React.Component {
  render(): React.ReactNode {
    return (
      <Layout style={{height: '100%'}}>
        <Header style={{backgroundColor: '#fff'}}>
          <Navbar/>
        </Header>
        <Content style={{padding: '50px'}}>
          {this.props.children}
        </Content>
      </Layout>
    );
  }
}
