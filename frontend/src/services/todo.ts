import client, {gql} from '../utils/graphql-client';
import {Todo} from '../modules/duck';

export const fetchTodoList = async () => client.query<{ todoList: ReadonlyArray<Todo> }>({
  query: gql`
      query {
          todoList {
              id
              text
              completed
          }
      }
  `
}).then(({data}) => data.todoList);

export const fetchChangeCompleted = (id: string) => client.mutate({
  mutation: gql`
      mutation Main($id: ID!) {
          todoSwitchCompleted(id: $id) {
              id
              text
              completed
          }
      }
  `,
  variables: {id}
}).then(({data}: any) => data.todoSwitchCompleted);

export const fetchClearCompletedTodo = () => client.mutate({
  mutation: gql`
      mutation {
          todoRemoveAllCompleted {
              id
          }
      }
  `,
})
.then(({data}: any) => data.todoRemoveAllCompleted);

export const fetchAddTodo = (text: string) => client.mutate({
  mutation: gql`
      mutation Main($text: String!) {
          todoCreate(text: $text) {
              id
              text
              completed
          }
      }
  `,
  variables: {text}
}).then(({data}: any) => data.todoCreate);

export const SubscribeTodoAdded = () => client.subscribe({
  query: gql`
      subscription {
          todoAdded {
              id
              text
              completed
          }
      }
  `
}).map(({data}: any) => data.todoAdded);

export const SubscribeTodoDeleted = () => client.subscribe({
  query: gql`
      subscription {
          todoDeleted {
              id
          }
      }
  `
}).map(({data}: any) => data.todoDeleted.map(({id}: any) => id));

export const SubscribeTodoUpdate = () => client.subscribe({
  query: gql`
      subscription {
          todoUpdate {
              id
              text
              completed
          }
      }
  `
}).map(({data}: any) => data.todoUpdate);

export const subscribeTodoAdded = SubscribeTodoAdded();
export const subscribeTodoDeleted = SubscribeTodoDeleted();
export const subscribeTodoUpdate = SubscribeTodoUpdate();
