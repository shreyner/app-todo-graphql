import * as React from 'react';
import {Provider} from 'react-redux';
import {render} from 'react-dom';
import {createBrowserHistory} from 'history';
import 'antd/dist/antd.css';

import configStore from './store';
import App from './containers/App';

const hostory = createBrowserHistory();

const {store} = configStore(hostory, {});

render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);
