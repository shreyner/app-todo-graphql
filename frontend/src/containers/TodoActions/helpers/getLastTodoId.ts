import {Todo} from '../../../modules/duck/todo';

export default (todos: ReadonlyArray<Todo>) => todos.reduce(
  (prevId, item) => parseInt(item.id, 10) >= prevId ? parseInt(item.id, 10) + 1 : prevId, 0
).toString();
