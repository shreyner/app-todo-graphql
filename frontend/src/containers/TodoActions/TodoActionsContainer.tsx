import * as React from 'react';
import {bindActionCreators, Dispatch} from 'redux';
import {connect} from 'react-redux';

import TodoForm from '../../components/TodoActions/TodoActions';
import {clearCompleted, fetchRequestAddTodo} from '../../modules/duck/todo';

export interface TodoFormContainerProps {
  addTodo: (text: string) => void;
  actionClearCompleted: () => void;
  lastId: string;
}

class TodoActionsContainer extends React.Component<TodoFormContainerProps, { value: string }> {
  state = {
    value: ''
  };

  render(): React.ReactNode {
    const {value} = this.state;

    return (
      <TodoForm
        value={value}
        onChange={this.handleChange}
        onAdd={this.handleAdd}
        clearCompleted={this.handleClearCompleted}
      />
    );
  }

  handleChange = (value: string) => this.setState({value});

  handleAdd = () => {
    this.props.addTodo(this.state.value);
    this.setState({value: ''});
  }

  handleClearCompleted = () => this.props.actionClearCompleted();
}

interface DispatchToProps {
  addTodo: (text: string) => void;
  actionClearCompleted: () => void;
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchToProps => ({
  addTodo: bindActionCreators(fetchRequestAddTodo, dispatch),
  actionClearCompleted: bindActionCreators(clearCompleted, dispatch)
});

export default connect(
  null,
  mapDispatchToProps
)(TodoActionsContainer);
