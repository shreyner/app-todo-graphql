// import * as React from 'react';

import {connect} from 'react-redux';
import App from '../components/App/App';
import {StateApplication} from '../store';

const mapStateToProps = ({location: {type}}: StateApplication) => ({
  typeNavigation: type
});

export default connect(mapStateToProps)(App);
