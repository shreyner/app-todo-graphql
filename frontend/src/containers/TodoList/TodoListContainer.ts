import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import TodoList from '../../components/TodoList/TodoList';
import {changeCompleteTodo, Todo} from '../../modules/duck/todo';
import {StateApplication} from '../../store';

interface StoreToProps {
  todoList: ReadonlyArray<Todo>;
}

const mapStoreToProps = ({todo}: StateApplication): StoreToProps => ({
  todoList: todo
});

interface DispatchToProps {
  completeTodo: (id: string) => void;
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchToProps => ({
  completeTodo: bindActionCreators(changeCompleteTodo, dispatch)
});

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(TodoList);
