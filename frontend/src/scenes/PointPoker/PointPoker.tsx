import * as React from 'react';
import {Row, Col} from 'antd';

import ChatLayout from '../../components/Chat/ChatLayout/ChatLayout';
import ChatSendForm from '../../components/Chat/ChatSendForm/ChatSendForm';
import ChatListMessages from '../../components/Chat/ChatListMessages/ChatListMessages';

export interface PointPokerProps {
}

export default class extends React.Component<PointPokerProps> {
  render(): React.ReactNode {
    return (
      <Row style={{height: '100%'}}>
        <Col span={16}>
          <h1>world</h1>
        </Col>
        <Col span={8} style={{height: '100%'}}>

          <ChatLayout>
            <ChatListMessages/>
            <ChatSendForm/>
          </ChatLayout>

        </Col>
      </Row>
    );
  }
}
