import * as React from 'react';
import {navigationActions} from '../../routes/routes';
import TodoMenu from '../../components/TodoMenu/TodoMenu';
import TodoList from '../../containers/TodoList/TodoListContainer';
import TodoActions from '../../containers/TodoActions/TodoActionsContainer';

export interface TodoProps {
  typeNavigation: string;
}

export default class extends React.Component<TodoProps> {
  render(): React.ReactNode {
    return (
      <>
        <TodoActions/>
        <TodoMenu/>
        {this.renderContent()}
      </>
    );
  }

  renderContent(): React.ReactNode {
    switch (this.props.typeNavigation) {
      case navigationActions.TODO_INDEX:
        return <TodoList/>;
      case navigationActions.TODO_SUCH:
        return <div>{navigationActions.TODO_SUCH}</div>;
      default:
        return '';
    }
  }
}
