export const navigationActions = {
  ROOT: '@navigation/root',
  POINT_POKER: '@navigation/point-poker',
  TODO_INDEX: '@navigation/todo',
  TODO_COMPLETED: '@navigation/todo-completed',
  TODO_SUCH: '@navigation/todo-such',
  TODO_NOT_COMPLETED: '@navigation/todo-not-completed',
};

export const routesMap = {
  [navigationActions.ROOT]: '/',
  [navigationActions.POINT_POKER]: '/:sessionId([a-zA-Z0-9]{16})',
  [navigationActions.TODO_INDEX]: '/todo',
  // [navigationActions.TODO_COMPLETED]: '/todo/completed',
  // [navigationActions.TODO_NOT_COMPLETED]: '/todo/not-completed',
  [navigationActions.TODO_SUCH]: '/todo/:id',
};

export const routesSaga = {
  // [routesMap[navigationActions.TODO_INDEX]]: function* suchTodo() {
  //   const list = yield call(fetchTodoList);
  //   yield put(updateTodo(list));
  // }
};
