import {createStore, compose, DeepPartial, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {connectRoutes, LocationState} from 'redux-first-router';

import {todo, Todo} from './modules/duck';
import {routesMap} from './routes/routes';
import {initMainSaga} from './modules/saga/mainSaga';

/// Вынести в отдельный модуль
export interface StateApplication {
  todo: ReadonlyArray<Todo>;
  location: LocationState;
}

// export default ;

export default function (history: any, initialStore: DeepPartial<StateApplication>) {
  const sagaMiddleware = createSagaMiddleware();

  const {reducer, middleware, enhancer} = connectRoutes(history, routesMap);

  const middlewares = [
    applyMiddleware(sagaMiddleware),
    applyMiddleware(middleware),
  ];

  const store = createStore(
    combineReducers<StateApplication>({
      todo,
      location: reducer as any
    }),
    initialStore,
    composeEnhancers(
      ...middlewares,
      enhancer,
    )
  );

  sagaMiddleware.run(initMainSaga(history));

  return {store};
}

const composeEnhancers = (...args: Array<any>) =>
  typeof window !== 'undefined'
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})(...args)
    : compose(...args);
