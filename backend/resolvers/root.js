const {AuthenticationError, BadUserInputError} = require('apollo-server');
const {PubSub, withFilter} = require('graphql-subscriptions');
const {camelCase} = require('lodash');
const {
  getListTodo,
  createTodo,
  switchStatusCompleted,
  removeAllCompleted
} = require('../models/todo');
const {generateToken} = require('../models/token');
const {createSession} = require('../models/session');
const {createMessage, listMessages} = require('../models/messages');


const pubSub = new PubSub();

const TODO_ADDED = 'TODO_ADDED';
const TODO_DELETED = 'TODO_DELETED';
const TODO_UPDATE = 'TODO_UPDATE';

module.exports = {
  Todo: {
    id: (todo) => todo._id,
  },
  Message: {
    id: (message) => message._id,
    userId: () => 'userId',
    user: () => ({}),
  },
  User: {
    name: () => 'Alexander',
  },
  Query: {
    todoList: () => getListTodo(),
    checkAuth: (_, {}, context) => {
      if (!context.authData) throw new AuthenticationError('Ты не авторизован');

      return true;
    },
    listMessages: (_, {sessionId}) => listMessages(sessionId),
  },
  Mutation: {
    getToken: (paren, {name}, context) => {
      if (!name) throw new BadUserInputError(
        'Invalid name',
        {invalidArgs: ['name']}
      );

      return generateToken({name});
    },
    todoCreate: async (_, {text}) => {
      const newTodo = await createTodo({text});

      pubSub.publish(TODO_ADDED, {[camelCase(TODO_ADDED)]: newTodo});
      return newTodo;
    },
    todoSwitchCompleted: async (_, {id}) => {
      const updateTodo = await switchStatusCompleted(id);
      pubSub.publish(TODO_UPDATE, {[camelCase(TODO_UPDATE)]: updateTodo});
      return updateTodo;
    },
    todoRemoveAllCompleted: async () => {
      const removedListTodo = await removeAllCompleted();
      pubSub.publish(TODO_DELETED, {[camelCase(TODO_DELETED)]: removedListTodo});
      return removedListTodo;
    },
    createSession: async () => createSession().then(value => {console.log(value);return value._id}),
    sendMessage: (_, {sessionId, text}) => {
      return createMessage({text, sessionId, typeUser: 'BASIC', typeMessage: 'MESSAGE', userId: '1'})
    },
  },
  Subscription: {
    todoAdded: {
      resolvers: payload => {
        console.log('resolvers', payload);
        return payload;
      },
      subscribe: withFilter(() => pubSub.asyncIterator(TODO_ADDED), (payload, variable) => {
        console.log(`subscribe ${counter++}`, payload, variable);
        return true;
      })
    },
    todoDeleted: {
      subscribe: () => pubSub.asyncIterator([TODO_DELETED])
    },
    todoUpdate: {
      subscribe: () => pubSub.asyncIterator([TODO_UPDATE])
    }
  }
};

let counter = 1;
