const {ApolloServer} = require('apollo-server');
const {verify} = require('./models/token');

const typeDefs = require('./schemas/root');
const resolvers = require('./resolvers/root');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({req, connection}) => {
    if (connection) {
      return {};
    } else {
      return {
        authData: !!req.headers['authorization-token'] && verify(req.headers['authorization-token']).payload
      };
    }
  }
});

server.listen().then(({url}) => {
  console.log(`🚀 Server ready at ${url}`);
});
