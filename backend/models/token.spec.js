const {generateToken, verify} = require('./token');

const token = generateToken({name: 'Alexander'});

console.log(token);

console.log(verify(token));

console.log(
  verify(
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQWxleGFuZGVyIiwiaWF0IjoxNTI5MzQ4MDEwfQ.JM9pUrMY0GZwtZgPzo1gDY8G-lq6TP9Rs4u9TYNJRUA'
  )
)

