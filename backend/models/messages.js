const DataStore = require('nedb');
const path = require('path');

const messagesStore = new DataStore({
  filename: path.join(__dirname, '../data/messagesStore.db')
});

messagesStore.loadDatabase(err => {
  if (err) throw new Error(err);

  console.info('Success load MessageStore')
});

/**
 * Message
 * @typedef {Object} Message
 * @property {string} _id
 * @property {string} typeMessage
 * @property {string} typeUser
 * @property {string} text
 * @property {string} userId
 * @property {string} sessionId
 */


/**
 *
 * @param {Message} message
 * @return {Promise<Message>}
 */
const createMessage = (message) => new Promise((resolve, reject) => {
  messagesStore.insert(message, (err, newDoc) => {
    if (!!err) reject(err);

    resolve(newDoc);
  })
});

const listMessages = (sessionId) => new Promise(resolve => {
  messagesStore.find({sessionId},  (err, listDoc) => {
    if (!!err) throw err;
    resolve(listDoc);
  })
});

module.exports = {
  messagesStore,
  listMessages,
  createMessage
};
