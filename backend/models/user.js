const DataStore = require('nedb');
const path = require('path');

const userStore = new DataStore({
    filename: path.join(__dirname, '..', 'data', 'userStore.db')
});

userStore.loadDatabase((err) => {
    if (err) throw new Error(err);
    console.info('Success load userStore');
});

/**
 * User
 * @typedef {Object} User
 * @property {string} name
 * @property {string} token
 */

/**
 *
 * @param {string} name
 * @return {Promise<User>}
 */
module.exports.createUser = (name) => new Promise((resolve, reject) => {
    if (!name) throw new Error('Not set name');

    // userStore.insert()
});
