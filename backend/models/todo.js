const DataStore = require('nedb');
const path = require('path');

const todoStore = new DataStore({
  filename: path.join(__dirname, '..', 'data', 'todoStore.db')
});

todoStore.loadDatabase((err) => {
  if (err) throw new Error(err);
  console.info('Success load todoStore')
});

/**
 * Todo
 * @typedef {Object} Todo
 * @property {string} _id
 * @property {string} text
 * @property {boolean} completed
 */

/**
 * Create new todo
 * @async
 * @param {Object} todo
 * @param {string} todo.text
 * @param {boolean=} todo.completed
 * @return {Promise<Todo>}
 */
const createTodo = (todo) => new Promise((resolve, reject) => {
  // Todo-Shreyner: Change error message
  if (!todo || !todo.text) throw new Error('todo');

  todoStore.insert({...todo, completed: !!todo.completed}, (err, newDoc) => {
    if (!!err) reject(err);

    resolve(newDoc);
  });
});

/**
 *
 * @return {Promise<Todo[]>}
 */
const find = (query = {}) => new Promise(resolve => {
  todoStore.find(query, (err, listDoc) => {
    if (!!err) throw err;
    resolve(listDoc);
  })
});

const remove = (query = {}, options = {}) => new Promise((resolve, reject) => {
  todoStore.remove(query, options, (err, numRemoved) => {
    if (!!err) reject(err);
    resolve(numRemoved)
  })
});

const getById = (id) => new Promise((resolve, reject) => {
  if (!id) throw new Error('Нету id');

  todoStore.findOne({_id: id}, (err, doc) => {
    if (err) throw new Error(err);

    resolve(doc);
  })
});

const switchStatusCompleted = (id) => new Promise((resolve) => {
  if (!id) throw new Error('Нету id');

  getById(id).then(doc => {
    todoStore.update({_id: id}, {$set: {completed: !doc.completed}}, {returnUpdatedDocs: true}, (err, numUpdate, updateDoc) => {
      resolve(updateDoc)
    })
  })
});

const removeAllCompleted = async () => {
  const removedTodo = await find({completed: true});
  await remove({completed: true}, {multi: true});
  return removedTodo;
};

/**
 * @async
 * @param {string} id
 * @return {Promise<number>} count deleted
 */
const deleteById = (id) => new Promise(resolve => {
  if (!id) throw new Error('id')

  todoStore.remove({_id: id}, (err, numRemoved) => {
    if (!!err) throw new Error(err);

    resolve(numRemoved);
  })
});

module.exports = {
  createTodo,
  getListTodo: find,
  getById,
  switchStatusCompleted,
  deleteById,
  removeAllCompleted,
  todoStore
};
