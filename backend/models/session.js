const DataStore = require('nedb');
const path = require('path');

const sessionStore = new DataStore({
  filename: path.join(__dirname, '../data/sessionStore.db')
});

sessionStore.loadDatabase(err => {
  if (err) throw new Error(err);

  console.info('Success load SessionStore')
});

const createSession = () => new Promise((resolve, reject) => {
  sessionStore.insert({}, (err, newDoc) => {
    if (!!err) reject(err);

    resolve(newDoc);
  })
});

module.exports = {
  sessionStore,
  createSession
};
