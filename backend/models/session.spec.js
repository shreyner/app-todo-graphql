const {createSession} = require('./session');

(async() => {
  console.log(await createSession())
})();
