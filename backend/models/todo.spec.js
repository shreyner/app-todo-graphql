const {
  createTodo,
  deleteById,
  getListTodo,
  switchStatusCompleted,
  getById,
  removeAllCompleted,
  todoStore,

} = require('./todo');

/**
 *
 * @param ms {number} Optional, default 1000
 * @return {Promise<boolean>}
 */
const delay = (ms) => new Promise(resolve => setTimeout(() => resolve(true), ms || 1000));

(async () => {
  // console.log(await getListTodo());
  //
  // const todo = await createTodo({text: 'Hello 1'});
  // const todo2 = await createTodo({text: 'Hello 2'});
  //
  // console.log(todo, todo2);
  //
  // console.log(await deleteById(todo._id));
  //
  // console.log(await getById(todo2._id));
  //
  // console.log(await switchStatusCompleted(todo2._id));
  //
  // console.log(await removeAllCompleted());

  // const listTodo = await getListTodo();
  //
  // console.log(listTodo);
  //
  // const allSwitchTodo = listTodo.map(item => switchStatusCompleted(item._id))
  //
  // await Promise.all(allSwitchTodo);
  //
  // console.log(await getListTodo());

  // console.log(await removeAllCompleted())
})();
