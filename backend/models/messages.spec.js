const {createMessage, listMessages} = require('./messages');

(async()=> {
  // "70UYG8m2SrxSc62S"
  // console.log(await createMessage({
  //   userId: '2',
  //   typeUser: 'BASIC',
  //   typeMessage: 'MESSAGE',
  //   text: 'hello 2',
  //   sessionId: '70UYG8m2SrxSc62S'
  // }));


  console.log(await listMessages("70UYG8m2SrxSc62S"))
})();
