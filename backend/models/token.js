const jwt = require('jsonwebtoken');
const config = require('../config');

/**
 * TokenPayload
 * @typedef {Object} TokenPayload
 * @property {string} name - User name
 */

/**
 *
 * @param {Object} options
 * @param {string} options.name
 */
module.exports.generateToken = ({name}) => jwt.sign({name}, config.get('jwtSecretKey'));

/**
 *
 * @param token
 * @return {Object} res
 * @return {boolean} res.verify
 * @return {TokenPayload|null} res.verify
 */
module.exports.verify = (token) => {
  try {
    return {verify: true, payload: jwt.verify(token, config.get('jwtSecretKey'))};
  } catch (e) {
    if (e instanceof jwt.JsonWebTokenError) {
      return {verify: false, payload: null}
    }

    throw e;
  }
};
