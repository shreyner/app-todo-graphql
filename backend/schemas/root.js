const {gql} = require('apollo-server');
const path = require('path');
const fs = require('fs');

module.exports = gql(fs.readFileSync(path.join(__dirname, './Root.graphqls'), 'utf-8'));
